#pragma once

double rand_double(double min_val, double max_val) {
	return ((rand() % 1000) / 1000.) * (max_val - min_val) + min_val;
}

void printString(char* s)
{
	glPushAttrib(GL_LIST_BIT);
	glListBase(fontOffset);
	glCallLists(strlen(s), GL_UNSIGNED_BYTE, (GLubyte*)s);
	glPopAttrib();
}
class Label {
public:
	GLfloat basePoint[2]; //middle of label
	GLfloat bHeight;
	GLfloat bWidth;
	GLfloat bgCol[3];
	GLfloat txCol[3];
	char text[160];
	int textLen;
	Label(GLfloat xm, GLfloat ym, GLfloat w, GLfloat h, char* text)
	{
		this->basePoint[0] = xm;
		this->basePoint[1] = ym;
		this->bHeight = h;
		this->bWidth = w;
		strcpy(this->text, text);
		this->textLen = strlen(text);
		this->bgCol[0] = 1.0;
		this->bgCol[1] = 0.9;
		this->bgCol[2] = 0.6;
		this->txCol[0] = 0.0;
		this->txCol[1] = 0.0;
		this->txCol[2] = 0.0;
	}
	void Draw()
	{
		glColor3f(bgCol[0], bgCol[1], bgCol[2]);
		glRectf(basePoint[0] - bWidth / 2, basePoint[1] - bHeight / 2,
			basePoint[0] + bWidth / 2, basePoint[1] + bHeight / 2);
		glColor3f(txCol[0], txCol[1], txCol[2]);
		glRasterPos2i(basePoint[0] - 5 * textLen, basePoint[1] - 5);
		printString(text);
	}
};
class Button {
public:
	GLfloat basePoint[2]; //middle of button
	GLfloat bHeight;
	GLfloat bWidth;
	GLfloat bgCol[3];
	GLfloat txCol[3];
	unsigned long int data[200];
	char* text;
	int textLen;
	void(*clickFunc)(unsigned long int*);
	Button(GLfloat xm, GLfloat ym, GLfloat w, GLfloat h, char* text, void(*function)(unsigned long int*))
	{
		this->basePoint[0] = xm;
		this->basePoint[1] = ym;
		this->bHeight = h;
		this->bWidth = w;
		this->text = text;
		this->textLen = strlen(text);
		this->clickFunc = function;
		this->bgCol[0] = 1.0;
		this->bgCol[1] = 0.5;
		this->bgCol[2] = 0.8;
		this->txCol[0] = 0.0;
		this->txCol[1] = 0.0;
		this->txCol[2] = 0.0;
	}
	void Draw()
	{
		glColor3f(bgCol[0], bgCol[1], bgCol[2]);
		glRectf(basePoint[0] - bWidth / 2, basePoint[1] - bHeight / 2,
			basePoint[0] + bWidth / 2, basePoint[1] + bHeight / 2);
		glColor3f(txCol[0], txCol[1], txCol[2]);
		glRasterPos2i(basePoint[0] - 5 * textLen, basePoint[1] - 5);
		printString(text);
	}
	void Click(int mouseX, int mouseY)
	{
		if ((abs(mouseX - basePoint[0]) < bWidth / 2) &&
			(abs(mouseY - basePoint[1]) < bHeight / 2))
		{
			clickFunc(data);
		}
	}
};
class Coordinate {
public:
	size_t x;
	size_t y;
	Coordinate() {}
	Coordinate(size_t x, size_t y)
	{
		this->x = x;
		this->y = y;
	}
};
class Room {
public:
	std::vector<Button> vecButtons;
	std::vector<Label> vecLabels;
	Room() {
	}
	void pushButton(Button btn) {
		this->vecButtons.push_back(btn);
	}
	void pushLabel(Label lbl) {
		this->vecLabels.push_back(lbl);
	}
	void deleteLabels(int start, int finish) {
		if(start < finish)
			this->vecLabels.erase(this->vecLabels.begin() + start, this->vecLabels.begin() + finish);
	}
	void deleteButtons(int start, int finish) {
		if (start < finish)
			this->vecButtons.erase(this->vecButtons.begin() + start, this->vecButtons.begin() + finish);
	}
};
class Cell {
private:
	double minerals;
	double water;
	std::vector<size_t> fungIndxs;
public:
	Cell(double m, double w)
	{
		this->minerals = m;
		this->water = w;
	}
	double takeMinerals(double C) {
		double res;
		if (C < this->minerals)
		{
			this->minerals -= C;
			res = C;
		}
		else
		{
			res = this->minerals;
			this->minerals = 0;
		}
		return res;
	}
	double takeWater(double C) {
		double res;
		if (C < this->water)
		{
			this->water -= C;
			res = C;
		}
		else
		{
			res = this->water;
			this->water = 0;
		}
		return res;
	}
	void addMinerals(double C) {
		double res = this->minerals + C;
		if (res > MAX_MINERALS)
			this->minerals = MAX_MINERALS;
		else
			this->minerals = res;
	}
	void addWater(double C) {
		double res = this->water + C;
		if (res > MAX_WATER)
			this->water = MAX_WATER;
		else
			this->water = res;
	}
	void setColor(void) {
		glColor3f(
			this->minerals * 0.65 / MAX_MINERALS + 0.35 - this->water * 0.2 / MAX_WATER,
			this->water * 0.1 / MAX_WATER + 0.25 + this->minerals * 0.1 / MAX_MINERALS,
			this->water * 0.5 / MAX_WATER + 0.2 - this->minerals * 0.15 / MAX_MINERALS
		);
	}
	void addFungus(size_t fungIndx) {
		bool exists = false;
		for (size_t i = 0; i < this->fungIndxs.size(); i++)
		{
			if (this->fungIndxs[i] == fungIndx)
			{
				exists = true;
				break;
			}
		}
		if (!exists)
			this->fungIndxs.push_back(fungIndx);
	}
	void removeFungus(size_t fungIndx)
	{
		int index = -1;
		for (size_t i = 0; i < this->fungIndxs.size(); i++)
		{
			if (this->fungIndxs[i] == fungIndx)
			{
				index = i;
				break;
			}
		}
		if (index > -1)
		{
			this->fungIndxs.erase(this->fungIndxs.begin()+index);
		}
	}
	bool hasFungus(size_t fungIndx)
	{
		for (size_t i = 0; i < this->fungIndxs.size(); i++)
		{
			if (this->fungIndxs[i] == fungIndx)
			{
				return true;
			}
		}
		return false;
	}
	std::vector<size_t>* list() {
		return &(this->fungIndxs);
	}
};

class Map {
private:
	std::vector<Cell> cells;
	size_t width;
	size_t height;
public:
	Map() {
		this->width = MapW / Square;
		this->height = MapH / Square;

		size_t side = 1;
		while (side + 1 < max(this->width, this->height)) {
			side *= 2;
		}
		side++;
		std::vector<std::vector<double>> water = Map::diamondSquare(side, 0., MAX_WATER, WATER_VAR);
		std::vector<std::vector<double>> minerals = Map::diamondSquare(side, 0., MAX_MINERALS, MINERAL_VAR);

		for (size_t i = 0; i < this->width * this->height; i++)
		{
			//Cell cell(i%4*100, i%7*100);
			Cell cell(minerals[i / (this->width)][i % (this->width)], water[i / (this->width)][i % (this->width)]);
			this->cells.push_back(cell);
		}
	}

	static std::vector<std::vector<double>> diamondSquare(size_t side, double min_val, double max_val, double var) {
		srand(time(0));
		std::vector<std::vector<double>> result;
		for (int i = 0; i < side; i++)
			result.push_back(std::vector<double>(side, 0));
		result[0][0] = rand_double(min_val, max_val);
		result[0][side - 1] = rand_double(min_val, max_val);
		result[side - 1][0] = rand_double(min_val, max_val);
		result[side - 1][side - 1] = rand_double(min_val, max_val);

		size_t cur_side = side;
		double cur_r = ROUGHNESS;
		double cur_var = var;
		while (cur_side > 2) {
			size_t ratio = side / (cur_side - 1);
			for (int i = 0; i < ratio; i++) {
				for (int j = 0; j < ratio; j++) {
					size_t x = j * (cur_side - 1);
					size_t y = i * (cur_side - 1);
					Map::diamond(&result, x + cur_side / 2, y + cur_side / 2, cur_side, cur_var);
				}
			}

			for (int i = 0; i < ratio; i++) {
				for (int j = 0; j < ratio; j++) {
					size_t x = j * (cur_side - 1);
					size_t y = i * (cur_side - 1);

					Map::square(&result, x, y + cur_side / 2, cur_side, cur_var);
					Map::square(&result, x + cur_side / 2, y, cur_side, cur_var);
					Map::square(&result, x + cur_side - 1, y + cur_side / 2, cur_side, cur_var);
					Map::square(&result, x + cur_side / 2, y + cur_side - 1, cur_side, cur_var);
				}
			}

			cur_side = cur_side / 2 + 1;
			cur_r *= ROUGHNESS;
			cur_var = var * cur_r;
		}

		return result;
	}

	static void square(std::vector<std::vector<double>>* map, size_t x, size_t y, size_t side, double var) {
		if ((*map)[y][x] == 0) {
			double sum = 0.;
			int count = 0;
			if (map->size() > x + side / 2) {
				sum += (*map)[y][x + side / 2];
				count++;
			}
			if (side / 2 <= x) {
				sum += (*map)[y][x - side / 2];
				count++;
			}
			if (map->size() > y + side / 2) {
				sum += (*map)[y + side / 2][x];
				count++;
			}
			if (side / 2 <= y) {
				sum += (*map)[y - side / 2][x];
				count++;
			}

			(*map)[y][x] = sum / count + rand_double(-var, var);
		}
	}

	static void diamond(std::vector<std::vector<double>>* map, size_t x, size_t y, size_t side, double var) {
		if (side > 2) {
			double middle = (
				(*map)[y - side / 2][x - side / 2] +
				(*map)[y - side / 2][x + side / 2] +
				(*map)[y + side / 2][x - side / 2] +
				(*map)[y + side / 2][x + side / 2]
				) / 4. + rand_double(-var, var);
			(*map)[y][x] = middle;
		}
	}
	void draw() {
		size_t x = this->width;
		for (size_t i = 0; i < this->width*this->height; i++)
		{
			this->cells[i].setColor();
			glRectf(
				MapL + Square * (i % x),
				MapT - Square * (i / x), 
				MapL + Square * (i % x) + Square, 
				MapT - Square * (i / x) - Square
			);
		}
	}
	Cell* getCell(size_t x, size_t y) {
		if ((y < this->height) && (x < this->width)) {
			return &(this->cells[y * this->width + x]);
		}
		else {
			return nullptr;
		}
	}

	Cell* getCell(Coordinate xy) {
		if ((xy.y < this->height) && (xy.x < this->width)) {
			return &(this->cells[xy.y * this->width + xy.x]);
		}
		else {
			return nullptr;
		}
	}
	size_t get_width() {
		return this->width;
	}

	size_t get_height() {
		return this->height;
	}

	bool isBorder(Coordinate xy, size_t ID) {
		if ((xy.x > 0) && !(this->getCell(xy.x - 1, xy.y)->hasFungus(ID))) {
			return true;
		}
		if ((xy.y > 0) && !(this->getCell(xy.x, xy.y - 1)->hasFungus(ID))) {
			return true;
		}
		if ((xy.x < this->width - 1) && !(this->getCell(xy.x + 1, xy.y)->hasFungus(ID))) {
			return true;
		}
		if ((xy.y < this->height - 1) && !(this->getCell(xy.x, xy.y + 1)->hasFungus(ID))) {
			return true;
		}
		return false;
	}

	std::vector<Coordinate> getFree(Coordinate xy, size_t ID) {
		std::vector<Coordinate> result;
		if ((xy.x > 0) && !(this->getCell(xy.x - 1, xy.y)->hasFungus(ID))) {
			result.push_back(Coordinate(xy.x - 1, xy.y));
		}
		if ((xy.y > 0) && !(this->getCell(xy.x, xy.y - 1)->hasFungus(ID))) {
			result.push_back(Coordinate(xy.x, xy.y - 1));
		}
		if ((xy.x < this->width - 1) && !(this->getCell(xy.x + 1, xy.y)->hasFungus(ID))) {
			result.push_back(Coordinate(xy.x + 1, xy.y));
		}
		if ((xy.y < this->height - 1) && !(this->getCell(xy.x, xy.y + 1)->hasFungus(ID))) {
			result.push_back(Coordinate(xy.x, xy.y + 1));
		}
		return result;
	}
};
