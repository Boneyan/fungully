#pragma once
template <class T, T const& MIN, T const& MAX>
class Gene {
    T val;
public:
    Gene(T v) {
        if (v < MIN) {
            val = MIN;
        }
        else if (v > MAX) {
            val = MAX;
        }
        else {
            val = v;
        }
    }

    Gene() {
        val = MIN;
    }

    T get() {
        return val;
    }

    Gene<T, MIN, MAX> mix(Gene<T, MIN, MAX> other, T var) {
        Gene<T, MIN, MAX> res((this->val + other.get()) / 2 + var);
        return res;
    }
};


long long int rand_int(long long int from, long long int to) {
    long long int diff = to - from;
    return rand() % diff + from;
}

unsigned int rand_uint(long long int from, long long int to) {
	long long int diff = to - from;
	return (unsigned int)(rand() % diff + from);
}

class DNA {
public:
    Gene<int, COLOR_MIN, COLOR_MAX> red_hat;
    Gene<int, COLOR_MIN, COLOR_MAX> green_hat;
    Gene<int, COLOR_MIN, COLOR_MAX> blue_hat;
    Gene<double, GROW_MIN, GROW_MAX> grow_speed;
    Gene<int, FUNGUS_SIZE_MIN, FUNGUS_SIZE_MAX> max_mycelium_size;
    Gene<int, AGE_MIN, AGE_MAX> max_age;
    Gene<int, SPORE_MNDIST_MIN, SPORE_MNDIST_MAX> spore_min_dist;
    Gene<int, SPORE_MXDIST_MIN, SPORE_MXDIST_MAX> spore_max_dist;
    
    DNA() {

        red_hat = Gene<int, COLOR_MIN, COLOR_MAX>(rand_uint(COLOR_MIN, COLOR_MAX));
        green_hat = Gene<int, COLOR_MIN, COLOR_MAX>(rand_uint(COLOR_MIN, COLOR_MAX));
        blue_hat = Gene<int, COLOR_MIN, COLOR_MAX>(rand_uint(COLOR_MIN, COLOR_MAX));

        grow_speed = Gene<double, GROW_MIN, GROW_MAX>(rand_double(GROW_MIN, GROW_MAX));

        max_mycelium_size = Gene<int, FUNGUS_SIZE_MIN, FUNGUS_SIZE_MAX>(rand_int(FUNGUS_SIZE_MIN, FUNGUS_SIZE_MAX));

        max_age = Gene<int, AGE_MIN, AGE_MAX>(rand_int(AGE_MIN, AGE_MAX));

        spore_min_dist = Gene<int, SPORE_MNDIST_MIN, SPORE_MNDIST_MAX>(rand_int(SPORE_MNDIST_MIN, SPORE_MNDIST_MAX));
        spore_max_dist = Gene<int, SPORE_MXDIST_MIN, SPORE_MXDIST_MAX>(rand_int(SPORE_MXDIST_MIN, SPORE_MXDIST_MAX));
    }

    DNA mix(DNA* other) {
        DNA child;
        child.red_hat = this->red_hat.mix(other->red_hat, rand_int(-COLOR_VAR, COLOR_VAR));
        child.blue_hat = this->blue_hat.mix(other->blue_hat, rand_int(-COLOR_VAR, COLOR_VAR));
        child.green_hat = this->green_hat.mix(other->green_hat, rand_int(-COLOR_VAR, COLOR_VAR));

        child.grow_speed = this->grow_speed.mix(other->grow_speed, rand_double(-GROW_VAR, GROW_VAR));

        child.max_mycelium_size = this->max_mycelium_size.mix(other->max_mycelium_size, rand_int(-FUNGUS_SIZE_VAR, FUNGUS_SIZE_VAR));

        child.max_age = this->max_age.mix(other->max_age, rand_int(-AGE_VAR, AGE_VAR));

        child.spore_min_dist = this->spore_min_dist.mix(other->spore_min_dist, rand_int(-SPORE_MNDIST_VAR, SPORE_MNDIST_VAR));
        child.spore_max_dist = this->spore_max_dist.mix(other->spore_max_dist, rand_int(-SPORE_MXDIST_VAR, SPORE_MXDIST_VAR));

        return child;
    }
};
enum state { Waiting, Fruiting, Immature };
class Mushroom {
public:
	size_t ID;
	int age;
	double sat;
	int health;
	double moist;
	state status;
	Coordinate coord;
	std::vector<Coordinate> mycelium;
	DNA genes;
	Mushroom() {}
	Mushroom(Coordinate xy, DNA dna)
	{
		this->ID = FUNG_ID;
		FUNG_ID++;
		this->age = 1;
		this->sat = MAX_SATIETY;
		this->health = MAX_HEALTH;
		this->moist = 0;
		this->status = Immature;
		this->coord = xy;
		this->genes = dna;
		addMycelium(xy);
	}
	void addMycelium(size_t x, size_t y)
	{
		Coordinate newc(x, y);
		this->mycelium.push_back(newc);
		Cell* changeCell = mapp.getCell(x, y);
		changeCell->addFungus(this->ID);
	}
	void addMycelium(Coordinate xy)
	{
		this->mycelium.push_back(xy);
		Cell* changeCell = mapp.getCell(xy);
		changeCell->addFungus(this->ID);
	}
	void removeMycelium(size_t x, size_t y)
	{
		Coordinate xy(x, y);
		removeMycelium(xy);
	}
	void removeMycelium(Coordinate xy)
	{
		int index = -1;
		for (size_t i = 0; i < this->mycelium.size(); i++)
		{
			if ((this->mycelium[i].x == xy.x) && (this->mycelium[i].y == xy.y))
			{
				index = i;
				break;
			}
		}
		if (index > -1)
		{
			this->mycelium.erase(this->mycelium.begin() + index);
			Cell* changeCell = mapp.getCell(xy);
			changeCell->removeFungus(this->ID);
		}
	}
	void eat(double minerals) {
		this->sat += minerals;
		if (this->sat > MAX_SATIETY) {
			this->sat = MAX_SATIETY;
		}
	}
	void drink(double water) {
		this->moist += water;
		if (this->moist > MAX_ACCUM_WATER) {
			this->moist = MAX_ACCUM_WATER;
		}
	}
	std::vector<size_t> neighbours() {
		std::vector<size_t> result;
		for (size_t i = 0; i < this->mycelium.size(); i++) {
			auto cell = mapp.getCell(this->mycelium[i]);
			std::vector<size_t>* list = cell->list();
			for (int j = 0; j < list->size(); j++)
				if ((*list)[j] != this->ID) {
					result.push_back((*list)[j]);
				}
		}
		return result;
	}
	double size() {
		return sqrt(this->age)* this->genes.grow_speed.get();
	}
	void draw()
	{
		double radius = this->size();
		GLfloat red = this->genes.red_hat.get() / 255.;
		GLfloat green = this->genes.green_hat.get() / 255.;
		GLfloat blue = this->genes.blue_hat.get() / 255.;
		glColor3f(red, green, blue);
		glBegin(GL_POLYGON);
		for (int i = 0; i < 60; i++)
		{
			glVertex2f(MapL + Square/2 + this->coord.x * Square + radius * multipliersx[i], 
				MapT - Square/2 - this->coord.y * Square + radius * multipliersy[i]);
		}
		glEnd();
	}
};