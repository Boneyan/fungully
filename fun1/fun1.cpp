﻿#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glu32.lib")

#include "Header.h"
#include "Values.h"
size_t FUNG_ID;
#include "Classes.h"
Map mapp;
#include "Fungus.h"

#define PIX 2.
#define MENUROOM 0
#define GAMEROOM 1
using namespace System;
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;
short int mAngle = 90;
bool wKey = false;
bool aKey = false;
bool sKey = false;
bool dKey = false;
bool leftTurn = false;
bool rightTurn = false;
bool amArrow = false;
bool PAUSED;
int playerState = 0;
int CURRENT_STEP = 0;
bool REPRODUCTION = false;
int REPRODUCTION_BEGIN = REPRODUCTION_PERIOD;
std::chrono::time_point<std::chrono::steady_clock> LAST_STEP;
std::vector<Mushroom> shrooms;
Mushroom* findShroom(size_t ID) {
	for (int i = 0; i < shrooms.size(); i++)
		if (shrooms[i].ID == ID)
			return &shrooms[i];
	return nullptr;
}

bool occupied(int x, int y) {
	for (int i = 0; i < shrooms.size(); i++)
		if ((shrooms[i].coord.x == x) && (shrooms[i].coord.y == y))
			return true;
	return false;
}

void makeRasterFont()
{
	GLuint i, j;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	fontOffset = glGenLists(128);
	for (i = 0, j = 'A';i < 26;i++, j++)
	{
		glNewList(fontOffset + j, GL_COMPILE);
		glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, letters[i]);
		glEndList();
	}
	for (i = 0, j = '0'; i < 10; i++, j++)
	{
		glNewList(fontOffset + j, GL_COMPILE);
		glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, chisels[i]);
		glEndList();
	}
	glNewList(fontOffset + ' ', GL_COMPILE);
	glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, space);
	glEndList();
	glNewList(fontOffset + '/', GL_COMPILE);
	glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, slashe);
	glEndList();
	glNewList(fontOffset + ':', GL_COMPILE);
	glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, colon);
	glEndList();
}
int currentRoom;
std::vector<Room> Rooms;
void clavIsFunc(unsigned char key, int x, int y)
{
	if (playerState == 0)
	{
		if (key == 'w')
		{
			wKey = true;
		}
		else if (key == 'a')
			aKey = true;
		else if (key == 'd')
			dKey = true;
		else if (key == 's')
			sKey = true;
		else if (key == '.')
		{
			leftTurn = true;
		}
		else if (key == ',')
		{
			rightTurn = true;
		}
		else if (key == ' ')
		{
			char eeks = 129;
			/*if (send(s, &eeks, 1, 0) < 0)
				exit(1);*/
			amArrow = true;
		}
		if (leftTurn)
		{
			mAngle -= 9;
			if (mAngle < 0)
				mAngle += 360;
		}
		if (rightTurn)
		{
			mAngle += 9;
			if (mAngle >= 360)
				mAngle -= 360;
		}

	}
}
void clavIsntFunc(unsigned char key, int x, int y)
{
	//claving off the clava
	if (key == 'w')
	{
		wKey = false;
		//glutKeyboardFunc(clavIsFunc);
	}
	else if (key == 'a')
		aKey = false;
	else if (key == 'd')
		dKey = false;
	else if (key == 's')
		sKey = false;
	else if (key == '.')
		leftTurn = false;
	else if (key == ',')
		rightTurn = false;
	//motionFunc(x, y);
}
#pragma region draw
void drawArrow(unsigned short int data, short int dx, short int dy)
{
	double realAngle = data * 3.1415 / 180.;
	int realX = WinW / 2 + (int)dx * PIX;
	int realY = WinH / 2 - (int)dy * PIX;
	glColor3f(1.0, 0.0, 1.0);
	glBegin(GL_POLYGON);
	glVertex2f(realX + 4.8 * PIX * cos(realAngle + 1.832), realY + 4.8 * PIX * sin(realAngle + 1.832));
	glVertex2f(realX + 9.5 * PIX * cos(realAngle) , realY + 9.5 * PIX * sin(realAngle));
	glVertex2f(realX + 4.8 * PIX * cos(realAngle - 1.832), realY + 4.8 * PIX * sin(realAngle - 1.832));
	glEnd();
}
void drawBody(short int dx, short int dy)
{
	int realX = WinW / 2 + (int)dx * PIX;
	int realY = WinH / 2 - (int)dy * PIX;
	glColor3f(0.78, 0.81, 0.72);
	glBegin(GL_POLYGON);
	glVertex2f(realX - 10 * PIX, realY);
	glVertex2f(realX, realY - 10 * PIX);
	glVertex2f(realX + 10 * PIX, realY);
	glVertex2f(realX, realY + 10 * PIX);
	glEnd();
}
void drawPlayer(unsigned short int data, short int dx, short int dy)
{
	drawBody(dx, dy);
	drawArrow(data, dx, dy);
}
void drawRectangle(unsigned long int wid, unsigned long int hei,
	unsigned char r, unsigned char g, unsigned char b, short int dx, short int dy)
{
	int realX = WinW / 2 + (int)dx * PIX;
	int realY = WinH / 2 - (int)dy * PIX;
	glColor3f(r/255., g/255., b/255.);
	glBegin(GL_POLYGON);
	glVertex2f(realX - wid / 2 * PIX, realY + hei / 2 * PIX);
	glVertex2f(realX - wid / 2 * PIX, realY - hei / 2 * PIX);
	glVertex2f(realX + wid / 2 * PIX, realY - hei / 2 * PIX);
	glVertex2f(realX + wid / 2 * PIX, realY + hei / 2 * PIX);
	glEnd();
}
void drawSpikes(short int dx, short int dy)
{
	int realX = WinW / 2 + (int)dx * PIX;
	int realY = WinH / 2 - (int)dy * PIX;
	glColor3f(0.6, 0.6, 0.6);
	glBegin(GL_POLYGON);
	glVertex2f(realX - 15 * PIX, realY + 15 * PIX);
	glVertex2f(realX - 15 * PIX, realY - 15 * PIX);
	glVertex2f(realX + 15 * PIX, realY - 15 * PIX);
	glVertex2f(realX + 15 * PIX, realY + 15 * PIX);
	glEnd();
}
void drawButton(short int dx, short int dy)
{
	int realX = WinW / 2 + (int)dx * PIX;
	int realY = WinH / 2 - (int)dy * PIX;
	glColor3f(0.02, 0.8, 0.02);
	glBegin(GL_POLYGON);
	glVertex2f(realX - 5 * PIX, realY + 5 * PIX);
	glVertex2f(realX - 5 * PIX, realY - 5 * PIX);
	glVertex2f(realX + 5 * PIX, realY - 5 * PIX);
	glVertex2f(realX + 5 * PIX, realY + 5 * PIX);
	glEnd();
}
void drawButtonPressed(short int dx, short int dy)
{
	int realX = WinW / 2 + (int)dx * PIX;
	int realY = WinH / 2 - (int)dy * PIX;
	glColor3f(0.02, 0.3, 0.02);
	glBegin(GL_POLYGON);
	glVertex2f(realX - 5 * PIX, realY + 5 * PIX);
	glVertex2f(realX - 5 * PIX, realY - 5 * PIX);
	glVertex2f(realX + 5 * PIX, realY - 5 * PIX);
	glVertex2f(realX + 5 * PIX, realY + 5 * PIX);
	glEnd();
}
void drawTimer(unsigned short int time)
{
	char text[30] = "LESS THAN 1 SEC";
	if (time > 0)
	{
		snprintf(text, 30, "%d : %d", time / 60, time % 60);
	}
	glColor3f(1., 1., 1.);
	glRasterPos2i(10, WinH - 28);
	printString(text);
}
#pragma endregion
#pragma region menu
void backFunc(unsigned long int* data)
{
	currentRoom = MENUROOM;
	glutPostRedisplay();
}
void newGameFunc(unsigned long int* data)
{
	char eeks = 3;
}
void exitFunc(unsigned long int* data)
{
	exit(0);
}
void startFunc(unsigned long int* data)
{
	mapp = Map();
	shrooms.clear();
	size_t mx, my;
	size_t w = mapp.get_width();
	size_t h = mapp.get_height();
	for (int i = 0; i < 30; i++)
	{
		mx = rand() % w;
		my = rand() % h;
		if (!occupied(mx, my)) {
			Coordinate mxy(mx, my);
			Mushroom shroom(mxy, DNA());
			shrooms.push_back(shroom);
		}
	}
	currentRoom = GAMEROOM;
	PAUSED = false;
	LAST_STEP = high_resolution_clock::now();
	CURRENT_STEP = 0;
	REPRODUCTION = false;
	REPRODUCTION_BEGIN = REPRODUCTION_PERIOD;

	glutPostRedisplay();
}
void pauseFunc(unsigned long* data)
{
	PAUSED = !PAUSED;
	Rooms[GAMEROOM].deleteButtons(1, 2);
	if (!PAUSED)
	{
		Button pauseButton(70, WinH - 145, 100., 70., "PAUSE", &pauseFunc);
		Rooms[GAMEROOM].pushButton(pauseButton);
	}
	else
	{
		Button unpauseButton(70, WinH - 145, 100., 70., "UNPAUSE", &pauseFunc);
		Rooms[GAMEROOM].pushButton(unpauseButton);
	}
	glutPostRedisplay();
}

void backFunc2(unsigned long int* data)
{
	Rooms[GAMEROOM].deleteLabels(0, Rooms[GAMEROOM].vecLabels.size());
	mAngle = 90;
	wKey = false;
	aKey = false;
	sKey = false;
	dKey = false;
	leftTurn = false;
	rightTurn = false;
	amArrow = false;
	playerState = 0;
	PAUSED = false;
	Rooms[GAMEROOM].deleteButtons(1, 2);
	Button pauseButton(70, WinH - 145, 100., 70., "PAUSE", &pauseFunc);
	Rooms[GAMEROOM].pushButton(pauseButton);
	//currentRoom = SESSROOM;
	//glutPostRedisplay();
}
#pragma endregion
void finishGame(void)
{
	unsigned short int time;
	unsigned char server_reply[2000];
	char infoText[303];
	char m;
	char nameLen;
	char playerName[255];
	char playerFate[3][10] = {"DEAD","FINISHED","STOPPED"};
	char state;
	unsigned short int timeStamp;
	server_reply[0] = 2; //вместо getByte
	server_reply[1] = 2; //вместо getByte
	time = 20; //вместо trCtUSI(server_reply, 0);
	m = 2; //вместо getByte
	for (int i = 0; i < m; i++)
	{
		nameLen = 2; //вместо getByte
		for (int j = 0; j < nameLen; j++)
		{
			playerName[j] = 2; //вместо getByte
		}
		playerName[nameLen] = 0;
		state = 2; //вместо getByte
		server_reply[2] = 2; //вместо getByte
		server_reply[3] = 2; //вместо getByte
		timeStamp = 20; //вместо trCtUSI(server_reply, 2);
		snprintf(infoText, 303, "%s / %d / %s / %d : %d", playerName, i + 1,
			playerFate[state], timeStamp / 60, timeStamp % 60);
		Label phLabel(WinW / 2, WinH - 110 - 66 * i, 395, 66, infoText);
		//Rooms[TABLROOM].pushLabel(phLabel);
	}
	snprintf(infoText, 303, "GAME IS FINISHED IN %d : %d", time / 60, time % 60);
	Label finishLabel(WinW / 2, WinH - 45, 345, 66, infoText);
	//Rooms[TABLROOM].pushLabel(finishLabel);
	glutPostRedisplay();
	m = 2; //вместо getByte
	if (m == 1)
	{
		Button backButton(45, WinH - 45, 66, 66, "BACK", &backFunc2);
		//Rooms[TABLROOM].pushButton(backButton);
		glutPostRedisplay();
	}
	else
	{
		printf("socket blocking error\n");
		exit(1);
	}
}

bool processShroom(size_t i) {
	shrooms[i].age++;
	// повзрослел ли
	if (shrooms[i].age == MATURE_AGE) {
		if (REPRODUCTION)
			shrooms[i].status = Fruiting;
		else
			shrooms[i].status = Waiting;
	}

	// умер ли от старости
	if (shrooms[i].age == shrooms[i].genes.max_age.get()) {
		return true;
	}

	// берёт минералы грибницей
	for (size_t j = 0; j < shrooms[i].mycelium.size(); j++) {
		double minerals = mapp.getCell(shrooms[i].mycelium[j])->takeMinerals(FEED);
		shrooms[i].eat(minerals);
	}

	// ест
	shrooms[i].sat -= LOOSE_SATIETY * sqrt(shrooms[i].age);
	if (shrooms[i].sat < 0) {
		// теряет здоровье если голодает
		shrooms[i].health += shrooms[i].sat;
		shrooms[i].sat = 0.;
		if (shrooms[i].health <= 0) {
			return true;
		}
	}

	// увеличение грибницы
	if ((shrooms[i].age % GROW_RATE == 0) &&
		(shrooms[i].sat >= GROW_SATIETY) &&
		(shrooms[i].mycelium.size() < shrooms[i].genes.max_mycelium_size.get()))
	{
		std::vector<size_t> candidates;
		for (size_t j = 0; j < shrooms[i].mycelium.size(); j++) {
			if (mapp.isBorder(shrooms[i].mycelium[j], shrooms[i].ID)) {
				candidates.push_back(j);
			}
		}

		if (candidates.size() > 0) {
			size_t candidate = rand_int(0, candidates.size());
			std::vector<Coordinate> frees = mapp.getFree(shrooms[i].mycelium[candidates[candidate]], shrooms[i].ID);
			size_t free = rand_int(0, frees.size());
			shrooms[i].addMycelium(frees[free]);
		}
	}

	// если плодоносит
	if (shrooms[i].status == Fruiting) {
		// берёт влагу грибницей
		for (size_t j = 0; j < shrooms[i].mycelium.size(); j++) {
			double water = mapp.getCell(shrooms[i].mycelium[j])->takeWater(DRINK);
			shrooms[i].drink(water);
		}

		// формирует спору
		if (shrooms[i].moist >= MAX_ACCUM_WATER) {
			std::vector<size_t> n = shrooms[i].neighbours();
			DNA* other;
			if (n.size() > 0) {
				size_t index = rand_int(0, n.size());
				other = &(findShroom(n[index])->genes);
			}
			else {
				other = &shrooms[i].genes;
			}
			DNA genes = shrooms[i].genes.mix(other);

			size_t radius = rand_int(shrooms[i].genes.spore_min_dist.get(), shrooms[i].genes.spore_max_dist.get());
			double angle = rand_double(0., 2. * 3.14159);
			int x = round(cos(angle) * radius) + (int)shrooms[i].coord.x;
			int y = round(sin(angle) * radius) + (int)shrooms[i].coord.y;
			if ((x >= 0) && (y >= 0) && (x < mapp.get_width()) && (y < mapp.get_height()) && (!occupied(x, y))) {
				Mushroom shroom(Coordinate(x, y), genes);
				shrooms.push_back(shroom);
			}
			shrooms[i].moist = 0.;
		}
	}

	return false;
}

void gameLoop(void)
{
	if (currentRoom == GAMEROOM)
	{
		if (!PAUSED)
		{
			auto now = high_resolution_clock::now();
			auto ms_int = duration_cast<milliseconds>(now - LAST_STEP);
			if (ms_int.count() >= STEP_GAP_MS) {
				LAST_STEP = high_resolution_clock::now();
				CURRENT_STEP++;

				if (!REPRODUCTION) {
					if (CURRENT_STEP == REPRODUCTION_BEGIN) {
						REPRODUCTION = true;
						for (size_t i = 0; i < shrooms.size(); i++) {
							if (shrooms[i].status == Waiting) {
								shrooms[i].status = Fruiting;
							}
						}
					}
				}
				else {
					if (CURRENT_STEP == REPRODUCTION_BEGIN + REPRODUCTION_LENGTH) {
						REPRODUCTION = false;
						REPRODUCTION_BEGIN = CURRENT_STEP + REPRODUCTION_PERIOD;
						for (size_t i = 0; i < shrooms.size(); i++) {
							if (shrooms[i].status == Fruiting) {
								shrooms[i].status = Waiting;
							}
						}
					}
					else {
						for (size_t x = 0; x < mapp.get_width(); x++)
							for (size_t y = 0; y < mapp.get_height(); y++)
								mapp.getCell(x, y)->addWater(WATER_RESTORE);
					}
				}

				std::vector<size_t> dead_ix;
				for (size_t i = 0; i < shrooms.size(); i++) {
					if (processShroom(i)) {
						dead_ix.push_back(i);
					}
				}

				// убрать умершие
				while (dead_ix.size() > 0) {
					size_t ix = dead_ix.back();
					dead_ix.pop_back();

					// оставляет минералы
					double minerals = shrooms[ix].size() * FEED * shrooms[ix].size();
					double per_cell = minerals / shrooms[ix].mycelium.size();
					Cell* shroomCell = mapp.getCell(shrooms[ix].coord);
					shroomCell->addMinerals(per_cell + MINERAL_AFTER_DEATH_MUSH);
					for (size_t j = 0; j < shrooms[ix].mycelium.size(); j++) {
						Cell* changeCell = mapp.getCell(shrooms[ix].mycelium[j]);
						if (changeCell != shroomCell) {
							changeCell->addMinerals(per_cell + MINERAL_AFTER_DEATH);
						}
						changeCell->removeFungus(shrooms[ix].ID);
					}

					shrooms.erase(shrooms.begin() + ix);
				}

				glutPostRedisplay();
			}
		}
	}
}

void Initialize() {
	currentRoom = MENUROOM;
	makeRasterFont();
	Room menuRoom;
	Button exitButton(WinW / 2, WinH / 3, 180., 80., "EXIT", &exitFunc);
	Button startButton(WinW / 2, WinH * 2 / 3, 180., 80., "START", &startFunc);
	menuRoom.pushButton(exitButton);
	menuRoom.pushButton(startButton);
	Rooms.push_back(menuRoom);
	Room gameRoom;
	Button backButton(70, WinH - 60, 100., 70., "BACK", &backFunc);
	Button pauseButton(70, WinH - 145, 100., 70., "PAUSE", &pauseFunc);
	//Label mapLabel(140+320, WinH/2, 640, 560, "I AM A MAP");
	gameRoom.pushButton(backButton);
	gameRoom.pushButton(pauseButton);
	//gameRoom.pushLabel(mapLabel);
	Rooms.push_back(gameRoom);
	glClearColor(0.9, 0.79, 0.4, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, WinW, 0, WinH, -1, 1);

	for (int i = 0; i < 60; i++)
	{
		multipliersx[i] = cos(3.14159 * 2 * i / 60);
		multipliersy[i] = sin(3.14159 * 2 * i / 60);
	}
}
void readClicks(int btn, int state, int x, int y)
{
	if ((btn == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN))
	{
		for (int i = 0; i < Rooms[currentRoom].vecButtons.size(); i++)
		{
			Rooms[currentRoom].vecButtons[i].Click(x, WinH - y);
		}
	}
}
void draw() {
	glClear(GL_COLOR_BUFFER_BIT);
	for (int i = 0; i < Rooms[currentRoom].vecButtons.size(); i++)
	{
		Rooms[currentRoom].vecButtons[i].Draw();
	}
	for (int i = 0; i < Rooms[currentRoom].vecLabels.size(); i++)
	{
		Rooms[currentRoom].vecLabels[i].Draw();
	}
	if (currentRoom == GAMEROOM)
	{
		mapp.draw();
		size_t l = shrooms.size();
		for (size_t i = 0; i < l; i++)
		{
			shrooms[i].draw();
		}
	}
	glFlush();
	glutSwapBuffers();
}
int main(array<System::String ^> ^args)
{
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(WinW, WinH);
	//glutInitWindowPosition(700, 100);
	glutCreateWindow("Fungully");
	Initialize();
	glutDisplayFunc(draw);
	glutIdleFunc(gameLoop);
	glutMouseFunc(readClicks);
	//glutPassiveMotionFunc(motionFunc);
	//glutMotionFunc(motionFunc);
	//glutSpecialFunc(motionFunc);
	glutKeyboardFunc(clavIsFunc);
	glutKeyboardUpFunc(clavIsntFunc);
	glutMainLoop();

	return 0;
}
